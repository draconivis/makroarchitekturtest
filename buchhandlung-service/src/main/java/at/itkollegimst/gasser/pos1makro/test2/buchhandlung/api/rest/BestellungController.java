package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.api.rest;

import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.BestellStatus;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.Bestellung;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.exceptions.BestellungNotFoundException;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.service.BestellungService;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.service.RabbitMQSender;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.shareddomain.events.BuchBestelltEvent;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/bestellung")
public class BestellungController {
	private BestellungService bestellungService;
	
	@Autowired
	private RabbitMQSender rabbitMQSender;
	
	@GetMapping("/{bestellNummer}")
	public ResponseEntity<Bestellung> getBestellung(@PathVariable String bestellNummer) throws BestellungNotFoundException {
		return new ResponseEntity<>(bestellungService.getBestellungByNummer(bestellNummer), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Bestellung>> getAllBestellungen() {
		return new ResponseEntity<>(bestellungService.getAllBestellungen(), HttpStatus.OK);
	}
	
	@PostMapping
	public void createBestellung(@RequestBody Bestellung bestellungToCreate) {
		bestellungService.createBestellung(bestellungToCreate);
		BuchBestelltEvent buchBestelltEvent = new BuchBestelltEvent(bestellungToCreate.getBestellNummer(), bestellungToCreate.getTitel());
		rabbitMQSender.send(buchBestelltEvent);
	}
	
	@GetMapping("/{bestellNummer}/status")
	public ResponseEntity<BestellStatus> getBestellStatus(@PathVariable String bestellNummer) throws BestellungNotFoundException {
		return new ResponseEntity<>(bestellungService.getBestellStatus(bestellNummer), HttpStatus.OK);
	}
	
	@PostMapping("/{bestellnummer}/status/{bestellStatus}")
	public void updateBestellStatus(@PathVariable String bestellnummer, @PathVariable BestellStatus bestellStatus) {
		bestellungService.updateBestellStatus(bestellnummer, bestellStatus);
	}
	
	@Transactional
	@DeleteMapping("/{bestellNummer}")
	public void deleteBestellung(@PathVariable String bestellNummer) {
		this.bestellungService.deleteBestellung(bestellNummer);
	}
	
}
