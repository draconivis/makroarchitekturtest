package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.service;

import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.BestellStatus;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.Bestellung;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.exceptions.BestellungNotFoundException;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.repository.BestellungRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BestellungService {
	private BestellungRepository bsRepo;
	
	public BestellungService(BestellungRepository bsRepo) {
		this.bsRepo = bsRepo;
	}
	
	public void createBestellung(Bestellung createBestellung) {
		Bestellung bestellung = new Bestellung(createBestellung);
		this.bsRepo.save(bestellung);
	}
	
	public Bestellung getBestellungByNummer(String bestellNummer) throws BestellungNotFoundException {
		return this.bsRepo.findByBestellNummer(bestellNummer).orElseThrow(BestellungNotFoundException::new);
	}
	
	public List<Bestellung> getAllBestellungen() {
		return this.bsRepo.findAll();
	}
	
	public void deleteBestellung(String bestellNummer) {
		this.bsRepo.deleteByBestellNummer(bestellNummer);
	}
	
	public BestellStatus getBestellStatus(String bestellNummer) throws BestellungNotFoundException {
		if (bsRepo.findByBestellNummer(bestellNummer).isPresent())
			return bsRepo.findByBestellNummer(bestellNummer).get().getStatus();
		else throw new BestellungNotFoundException();
	}
	
	public void updateBestellStatus(String bestellNummer, BestellStatus bestellStatus) {
		if (bsRepo.findByBestellNummer(bestellNummer).isPresent()) {
			Bestellung bestellung = bsRepo.getByBestellNummer(bestellNummer).get();
			bestellung.setStatus(bestellStatus);
			bsRepo.save(bestellung);
		}
	}
}
