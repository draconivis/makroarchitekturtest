package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.shareddomain.events;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, scope = BuchBestelltEvent.class)
public class BuchBestelltEvent implements Serializable {
	private String bestellNummer;
	private String bestellTitel;
}
