package at.itkollegimst.gasser.pos1makro.test2.druckerei.shareddomain.model;

import lombok.Data;

@Data
public class Bestellung {
	
	private String bestellNummer;
	private String titel;
	
	public Bestellung() {
	}
	
	public Bestellung(String bestellNummer, String bestellTitel) {
		this.bestellNummer = bestellNummer;
		this.titel = bestellTitel;
	}
	
	public boolean print() {
		try {
			wait(5000);
		} catch (InterruptedException ignored) {}
		return true;
	}
}
