package at.itkollegimst.gasser.pos1makro.test2.druckerei.shareddomain.events;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, scope = BuchGedrucktEvent.class)
public class BuchGedrucktEvent {
	private String bestellNummer;
	private String bestellTitel;
}
