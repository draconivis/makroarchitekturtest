package at.itkollegimst.gasser.pos1makro.test2.druckerei.service;

import at.itkollegimst.gasser.pos1makro.test2.druckerei.shareddomain.events.BuchBestelltEvent;
import at.itkollegimst.gasser.pos1makro.test2.druckerei.shareddomain.model.Bestellung;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RabbitMQConsumer implements RabbitListenerConfigurer {
	
	private static final Logger logger = LoggerFactory.getLogger(RabbitMQConsumer.class);
	
	@Override
	public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
	}
	
	@RabbitListener(queues = "${spring.rabbitmq.queue}")
	public void bestellungCreatedEventMessageReceived(BuchBestelltEvent buchBestelltEvent) {
		 logger.info(buchBestelltEvent.getBestellTitel());
		//logger.info("Bestellung " + buchBestelltEvent.getBestellTitel() + " wird gedruckt");
		//Bestellung bestellung = new Bestellung(buchBestelltEvent.getBestellNummer(), buchBestelltEvent.getBestellTitel());
		//if (bestellung.print())
			//rabbitMQSender.send(new BuchGedrucktEvent(bestellung.getBestellNummer(), bestellung.getTitel()));
	}
	
}
