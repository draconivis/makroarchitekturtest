package at.itkollegimst.gasser.pos1makro.test2.druckerei.shareddomain.model;

public enum BestellStatus {
	BESTELLT, ABHOLBEREIT
}
